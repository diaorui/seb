/* miniball.f -- translated by f2c (version 20090411).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "f2c.h"

/* Table of constant values */

static integer c__9 = 9;
static integer c__1 = 1;
static integer c__5 = 5;


/*    computing the smallest enclosing ball of a set of points */
/*    in n-dimensional space */

/* Main program */ int MAIN__(void)
{
    /* Format strings */
    static char fmt_16[] = "(/5x,\002Solving Miniball problems using LBFGS m"
	    "athod.\002/)";

    /* System generated locals */
    integer i__1, i__2;

    /* Builtin functions */
    integer s_wsfe(cilist *), e_wsfe(void);
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_cmp(char *, char *, ftnlen, ftnlen);
    double sqrt(doublereal), exp(doublereal), log(doublereal);
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void);
    /* Subroutine */ int s_stop(char *, ftnlen);

    /* Local variables */
    static doublereal c__[50000000], e[10000], f, g[5000];
    static integer i__, j;
    static doublereal l[5000];
    static integer m, n;
    static doublereal p;
    static integer s;
    static doublereal u[5000], x[5000], t1, t2, t3, ci[5000], wa[193315];
    static integer nbd[5000], iwa[15000];
    static char task[60];
    static doublereal factr;
    static char csave[60];
    static doublereal dsave[29];
    static integer isave[44];
    static logical lsave[4];
    static doublereal pgtol;
    extern /* Subroutine */ int setulb_(integer *, integer *, doublereal *, 
	    doublereal *, doublereal *, integer *, doublereal *, doublereal *,
	     doublereal *, doublereal *, doublereal *, integer *, char *, 
	    integer *, char *, logical *, integer *, doublereal *, ftnlen, 
	    ftnlen);
    static integer iprint;

    /* Fortran I/O blocks */
    static cilist io___13 = { 0, 6, 0, fmt_16, 0 };
    static cilist io___30 = { 0, 6, 0, 0, 0 };
    static cilist io___31 = { 0, 6, 0, 0, 0 };


/*     The dimension n of this */
/*     problem is variable. */
/*     nmax  is the dimension of the largest problem to be solved. */
/*     mmax  is the maximum number of limited memory corrections. */
/*     lenwa is the corresponding real workspace required. */
/*     Declare the variables needed by the code. */
/*     A description of all these variables is given at the end of */
/*     the driver. */
/*     Declare a few additional variables for this sample problem. */
/*     We wish to have output at every iteration. */
    iprint = 1;
/*     We specify the tolerances in the stopping criteria. */
    factr = 1e7;
    pgtol = 1e-5;
/*     We specify the dimension n of the sample problem and the number */
/*     m of limited memory corrections stored.  (n and m should not */
/*     exceed the limits nmax and mmax respectively.) */
    n = 200;
    s = 1000;
    m = 7;
/*    smoothing parameter */
    p = .1;
/*    generate the centers and radius randomly */
    t1 = 7.;
    i__1 = s * n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	t1 = t1 * 445. + 1;
	while(4096. <= t1) {
	    t1 += -4096.;
	}
	c__[i__ - 1] = t1 / 40.96;
/* L5: */
    }
/*    x(i) is unbounded. */
    i__1 = n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	nbd[i__ - 1] = 0;
/* L10: */
    }
/*     We now define the starting point. */
    i__1 = n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	x[i__ - 1] = 0.;
/* L14: */
    }
/*     We now write the heading of the output. */
    s_wsfe(&io___13);
    e_wsfe();
/*     We start the iteration by initializing task. */
    s_copy(task, "START", (ftnlen)60, (ftnlen)5);
/*     ------- The beginning of the loop ---------- */
L111:
/*     This is the call to the L-BFGS-B code. */
    setulb_(&n, &m, x, l, u, nbd, &f, g, &factr, &pgtol, wa, iwa, task, &
	    iprint, csave, lsave, isave, dsave, (ftnlen)60, (ftnlen)60);
    if (s_cmp(task, "FG", (ftnlen)2, (ftnlen)2) == 0) {
/*        The minimization routine has returned to request the */
/*        function f and gradient g values at the current x. */
/*        Compute function value f. */
	t3 = 0.;
	i__1 = s;
	for (i__ = 1; i__ <= i__1; ++i__) {
	    t2 = 0.;
	    i__2 = n;
	    for (j = 1; j <= i__2; ++j) {
		ci[j - 1] = c__[(i__ - 1) * n + j - 1];
		ci[j - 1] = x[j - 1] - ci[j - 1];
		t2 += ci[j - 1] * ci[j - 1];
/* L18: */
	    }
	    if (t3 <= sqrt(t2)) {
		t3 = sqrt(t2);
	    }
	    t2 += p * p;
	    e[i__ - 1] = sqrt(t2);
/* L19: */
	}
	t1 = 0.;
	i__1 = s;
	for (i__ = 1; i__ <= i__1; ++i__) {
	    if (t1 <= e[i__ - 1]) {
		t1 = e[i__ - 1];
	    }
/* L20: */
	}
	t2 = 0.;
	i__1 = s;
	for (i__ = 1; i__ <= i__1; ++i__) {
	    e[i__ - 1] = exp((e[i__ - 1] - t1) / p);
	    t2 += e[i__ - 1];
/* L21: */
	}
	f = t1 + p * log(t2);
/*        Compute gradient g for the sample problem. */
	i__1 = s;
	for (i__ = 1; i__ <= i__1; ++i__) {
	    e[i__ - 1] /= t2;
/* L22: */
	}
	i__1 = n;
	for (i__ = 1; i__ <= i__1; ++i__) {
	    g[i__ - 1] = 0.;
/* L23: */
	}
	i__1 = s;
	for (i__ = 1; i__ <= i__1; ++i__) {
	    t2 = 0.;
	    i__2 = n;
	    for (j = 1; j <= i__2; ++j) {
		ci[j - 1] = c__[(i__ - 1) * n + j - 1];
		ci[j - 1] = x[j - 1] - ci[j - 1];
		t2 += ci[j - 1] * ci[j - 1];
/* L24: */
	    }
	    t2 = sqrt(t2 + p * p);
	    i__2 = n;
	    for (j = 1; j <= i__2; ++j) {
		g[j - 1] += e[i__ - 1] * ci[j - 1] / t2;
/* L25: */
	    }
/* L26: */
	}
/*        Go back to the minimization routine. */
	goto L111;
    } else if (s_cmp(task, "NEW_X", (ftnlen)5, (ftnlen)5) == 0) {
/*        The minimization routine has returned with a new iterate, */
/*        and we have opted to continue the iteration. */
	goto L111;
    } else {
	if (p >= 1.1e-6) {
	    p /= 10;
	    goto L111;
	}
/*        We terminate execution when task is neither FG nor NEW_X. */
/*        We print the information contained in the string task */
/*        if the default output is not used and the execution is */
/*        not stopped intentionally by the user. */
	if (iprint <= -1 && s_cmp(task, "STOP", (ftnlen)4, (ftnlen)4) != 0) {
	    s_wsle(&io___30);
	    do_lio(&c__9, &c__1, task, (ftnlen)60);
	    e_wsle();
	}
	s_wsle(&io___31);
	do_lio(&c__9, &c__1, " f(x^k) =   ", (ftnlen)12);
	do_lio(&c__5, &c__1, (char *)&t3, (ftnlen)sizeof(doublereal));
	e_wsle();
    }
/*     ---------- The end of the loop ------------- */
    s_stop("", (ftnlen)0);
    return 0;
} /* MAIN__ */

/* Main program alias */ int miniball_ () { MAIN__ (); return 0; }
