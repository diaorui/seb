#ifndef __LA_H__
#define __LA_H__

double inner_product(int, double *, double *);
double max_val(const int, double*);
double max_abs(const int, double*);
double sumup(const int, double *);
double sumup_s(const int, int *, double *);
#endif
