#ifndef __TRUNCATE_NEWTON_CG_H__
#define __TRUNCATE_NEWTON_CG_H__

double truncate_newton_cg(const int m, const int n, double *x, double *r, int *c_rowptr, int *c_col, double *c_val);
void truncate_newton_cg_sub(double *x, double mu, double *r, int *c_rowptr, int *c_col, double *c_val, const int m, const int n, double s_bound, void *memalloc);

#endif
