#ifndef __CG_H__
#define __CG_H__

void cg(const int n, const int size_s, int *s, int *c_rowptr, int *c_col, double *c_val, double *x, double *g, double mu, double *lambda, double *tilde_grad_f, const double norm_tilde_grad_f, void *memalloc, double *direction) ;

#endif
