#ifndef __LINE_SEARCH_H__
#define __LINE_SEARCH_H__

void line_search(const int n, const int m, double *x, double *direction, double *tilde_grad_f, int *c_rowptr, int *c_col, double *c_val, const double mu, double *g, double *r, double *f_comp, double *max_f_comp, double *exp_f, const double c1, const double f, double *f_new, double *x_new);

#endif
