[include "la.h"
#include <math.h>
#include "alloc.h"
#include "util.h"
#include <memory.h>
#include <stdio.h>

static union{
	double d;
	struct{
		int j,i;
		} n;
} d2i;
#define EXP_A (1048576/M_LN2)
#define EXP_C 60801
//#define fastexp(y) (d2i.n.i = EXP_A*(y)+(1072693248-EXP_C),d2i.d)

double fastexp(double c) {
	return exp(c);
}

void calc_g(const int n, const int m, double *x, int *c_rowptr, int *c_col, double *c_val, const double mu2, double *g) {
	int i, j;

	double sum_x = 0;
	for (j = 0; j < n; j++) {
		sum_x += x[j] * x[j];
	}

	for (i = 0; i < m; i++) {
		double sum = sum_x;
		for (j = c_rowptr[i]; j < c_rowptr[i + 1]; j++) {
			double tmp = x[c_col[j]] - c_val[j];
			sum += tmp * tmp - x[c_col[j]] * x[c_col[j]];
		}
		g[i] = sqrt(sum + mu2);
	}
}

void calc_f_comp(const int m, double *g, double *r, double *f_comp) {
	int i;
	for (i = 0; i < m; i++) {
		f_comp[i] = g[i] + r[i];
	}
}

void calc_s(const int m, const double max_f_comp, double *f_comp, const double bound, int *size_s, int *s) {
	int i;

	*size_s = 0;
	
	for (i = 0; i < m; i++) {
		if (f_comp[i] > bound + max_f_comp) {
			s[(*size_s)++] = i;
		}
	}
}

void calc_exp_f(const int m, const double max_f_comp, double *f_comp, const double mu, double *exp_f) {
	int i;
	for (i = 0; i < m; i++) {
		exp_f[i] = fastexp((f_comp[i] - max_f_comp) / mu);
	}
}

void calc_exp_f_s(const int size_s, int *s, const double max_f_comp, double *f_comp, const double mu, double *exp_f) {
	int i;
	for (i = 0; i < size_s; i++) {
		exp_f[s[i]] = fastexp((f_comp[s[i]] - max_f_comp) / mu);
	}
}

void calc_lambda(const int size_s, int *s, double *exp_f, const double sum_exp_f_s, double *lambda) {
	int i;
	for (i = 0; i < size_s; i++) {
		lambda[i] = exp_f[s[i]] / sum_exp_f_s;
	}
}

void calc_tilde_grad_f(const int n, const int size_s, int *s, double *g, double *x, int *c_rowptr, int *c_col, double *c_val, double *lambda, double *tilde_grad_f) {
	int i, j;

	//memset(tilde_grad_f, 0, sizeof(tilde_grad_f[0]) * n);

	double sum = 0;
	for (i = 0; i < size_s; i++) {
		sum += lambda[i] / g[s[i]];
	}
	for (j = 0; j < n; j++) {
		tilde_grad_f[j] = x[j] * sum;
	}
	
	for (i = 0; i < size_s; i++) {
		for (j = c_rowptr[s[i]]; j < c_rowptr[s[i] + 1]; j++) {
			tilde_grad_f[c_col[j]] -= c_val[j] / g[s[i]] * lambda[i];
		}	
	}
}

double calc_f(const int n, const int m, double *x, int *c_rowptr, int *c_col, double *c_val, double *r) {
	int i, j;
	double res = 0;
	double sum_x = 0;
	for (j = 0; j < n; j++) {
		sum_x += x[j] * x[j];
	}


	for (i = 0; i < m; i++) {
		double val = sum_x;
		for (j = c_rowptr[i]; j < c_rowptr[i + 1]; j++) {
			double tmp = x[c_col[j]] - c_val[j];
			val += tmp * tmp - x[c_col[j]] * x[c_col[j]];
		}
		val = sqrt(val) + r[i];
		if (res < val) {
			res = val;
		}
	}
	return res;
}

void calc_hessian_coeff(const int size_s, int *s, double *g, const double mu, double *lambda, double *hessian_coeff1, double *hessian_coeff2) {
	int i;

	*hessian_coeff2 = 0;
	for (i = 0; i < size_s; i++) {
		double tmp = lambda[i] / g[s[i]];
		*hessian_coeff2 += tmp;
		hessian_coeff1[i] = (1. / mu - 1. / g[s[i]]) * tmp / g[s[i]];
	}
}

double calc_residual_bound(const int n, const double norm) {
	double sqrt_norm = sqrt(norm);
	if (sqrt_norm < 0.25) {
		return sqrt_norm * norm;
	}
	else {
		return 0.25 * norm;
	}
}

void hessian_mul_vector(const int n, const int size_s, int *s, double mu, double *tilde_grad_f, int *c_rowptr, int *c_col, double *c_val, double *x, double *v, double* hessian_coeff1, const double hessian_coeff2, double *hessian_v) {
	int i, j;
	double tmp;
	double sum_tmp = 0;
	double xv = 0;

	tmp = 0;
	for (j = 0; j < n; j++) {
		tmp += tilde_grad_f[j] * v[j];
	}
	tmp /= - mu;
	for (j = 0; j < n; j++) {
		hessian_v[j] = tmp * tilde_grad_f[j];
	}
	for (j = 0; j < n; j++) {
		xv += x[j] * v[j];
	}

	for (i = 0; i < size_s; i++) {
		tmp = xv;
		for (j = c_rowptr[s[i]]; j < c_rowptr[s[i] + 1]; j++) {
			tmp -= c_val[j] * v[c_col[j]];
		}
		tmp *= hessian_coeff1[i];
		sum_tmp += tmp;
		for (j = c_rowptr[s[i]]; j < c_rowptr[s[i] + 1]; j++) {
			hessian_v[c_col[j]] -= tmp * c_val[j];
		}
	}
	for (j = 0; j < n; j++) {
		hessian_v[j] += sum_tmp * x[j];
	}
	
	for (j = 0; j < n; j++) {
		hessian_v[j] += hessian_coeff2 * v[j];
	}
}

