#include <stdio.h>
#include <malloc.h>
#include <memory.h>
#include <math.h>
#include <time.h>
#include "gen_problem.h"
#include "truncate_newton_cg.h"
#include "util.h"
#include <assert.h>
#include <string.h>

double P=1;
/*

p=10
11911 11982 0.994074
1922 1984 0.968750

P=5
11911 11982 0.994074
1926 1984 0.970766

P=3
11911 11982 0.994074
1925 1984 0.970262

P=1
11911 11982 0.994074
1924 1984 0.969758

P=0.1
11911 11982 0.994074
1920 1984 0.967742
*/
/**
void run_test(int m, int n) {
	clock_t t1 = clock(), t2;

	double *r, *C;
	int i;
	double *x;
	double y;
	double timing;

	gen_problem(&m, &n, &r, &C);

	x = (double*)malloc(sizeof(double) * n);
	for (i = 0; i < n; i++) {
		x[i] = 0;
	}

	y = truncate_newton_cg(m, n, x, r, C);

	free(x);
	free(C);
	free(r);
	
	t2 = clock();	
	timing = (double) (t2 - t1) / (double) CLOCKS_PER_SEC;
	printf("%8d%8d  %.10e  %.10e\n", m, n, timing, y);

}

int mlist1[] = {1000,1000,1000,2000,3000,4000,5000,6000,7000,8000,9000,10000,20000};
int nlist1[] = {400,800,1000,1000,1000,1000,1000,1000,1000,1000,1000,1000,1000};

int mlist2[] = {8000,16000,32000,64000,128000,256000,512000};
int nlist2[] = {100,100,100,100,100,100,100};

int mlist3[] = {1000,2000,3000,4000,5000,6000,7000,8000,9000,10000};
int nlist3[] = {2000,2000,2000,2000,2000,2000,2000,2000,2000,2000};

int mlist4[] = {2000,2000,2000,2000,2000,2000,2000,2000,5000,5000,5000,5000,5000,5000,5000,5000,8000,10000,10000};
int nlist4[] = {3000,4000,5000,6000,7000,8000,9000,10000,3000,4000,5000,6000,7000,8000,9000,10000,7000,8000,10000};

int mlist5[] = {30000, 40000, 50000, 100000, 1024000, 2048000, 20000, 30000, 40000, 50000, 100000, 20000};
int nlist5[] = {1000, 1000, 1000, 1000, 100, 100, 2000, 2000, 2000, 2000, 2000, 10000};
*/
/**
int main() {

	int i;

	printf("%8s%8s%12s%12s\n", "m", "n", "cpu", "y");
	for (i = 0; i < sizeof(mlist1) / sizeof(int); i++) {
		run_test(mlist1[i],nlist1[i]); 
	}

	printf("%8s%8s%12s%12s\n", "m", "n", "cpu", "y");
	for (i = 0; i < sizeof(mlist2) / sizeof(int); i++) {
		run_test(mlist2[i],nlist2[i]); 
	}
	
	printf("%8s%8s%12s%12s\n", "m", "n", "cpu", "y");
	for (i = 0; i < sizeof(mlist3) / sizeof(int); i++) {
		run_test(mlist3[i],nlist3[i]); 
	}
	printf("%8s%8s%12s%12s\n", "m", "n", "cpu", "y");
	for (i = 0; i < sizeof(mlist4) / sizeof(int); i++) {
		run_test(mlist4[i],nlist4[i]); 
	}
	printf("%8s%8s%12s%12s\n", "m", "n", "cpu", "y");
	for (i = 0; i < sizeof(mlist5) / sizeof(int); i++) {
		run_test(mlist5[i],nlist5[i]); 
	}
	return 0;
}
*/


int mnist38() {
	int n_sample = 11982;
	int n_feature = 780;
	int n_avg = 180;

	char line[10000];
	FILE *f = fopen("../Data/mnist.scale", "r");

	
	int m = 0;
	int n = n_feature + 1 + n_sample;
	double *x = (double*)malloc(sizeof(double) * n);
	double y;
	int i, j;
	double *r = (double*)malloc(sizeof(double) * n_sample);
	int *c_rowptr = (int*)malloc(sizeof(int) * (n_sample + 1));
	int *c_col = (int*)malloc(sizeof(int) * n_avg * n);
	double *c_val = (double*)malloc(sizeof(double) * n_avg * n);
	int correct = 0;
	int total = 0;

	memset(r, 0, sizeof(double) * n_sample);
	memset(x, 0, sizeof(double) * n);
	c_rowptr[0] = 0;
	
	while (fgets(line, 10000, f)) {
		int label;
		char *token = strtok(line, " \n");
		int key;
		double value;
		double norm = 0;
		sscanf(token, "%d", &label);
		if (label != 3 && label != 8) continue;
		if (label == 3) {
			label = 1;
		}
		else {
			label = -1;
		}
		c_rowptr[m + 1] = c_rowptr[m];
		token = strtok(NULL, " \n");
		while (token) {
			sscanf(token, "%d:%lf", &key, &value);
			token = strtok(NULL, " \n");
			assert(key >= 1 && key <= n_feature);
			c_val[c_rowptr[m + 1]] = label * value;
			c_col[c_rowptr[m + 1]] = key - 1;
			norm += value * value;
			c_rowptr[m + 1]++;
		}
		norm = sqrt(norm);
		for (i = c_rowptr[m]; i < c_rowptr[m + 1]; i++) {
			c_val[i] /= norm;
		}
		c_val[c_rowptr[m + 1]] = label;
		c_col[c_rowptr[m + 1]] = n_feature;
		c_rowptr[m + 1]++;
		c_val[c_rowptr[m + 1]] = 1. / sqrt(P);
		c_col[c_rowptr[m + 1]] = n_feature + 1 + m;
		c_rowptr[m + 1]++;
		m++;
		
	}
	
	assert(m == n_sample);
	fclose(f);
	
	for (i = 0; i < n; i++) {
		x[i] = 0;
	}

	y = truncate_newton_cg(m, n, x, r, c_rowptr, c_col, c_val);

	for (i = 0; i < m; i++) {
		double test = 0;
		for (j = c_rowptr[i]; j < c_rowptr[i + 1]; j++) {
			if (c_col[i] > n_feature) continue;
			test += c_val[j] * x[c_col[j]];
		}
		if (test > 0) correct++;
		total++;
	}
	printf("%d %d %f\n", correct, total, correct * 1. / total);

	correct = total = 0;
	f = fopen("../Data/mnist.scale.t", "r");
	while (fgets(line, 10000, f)) {
		int label;
		char *token = strtok(line, " \n");
		int key;
		double value;
		double norm = 0;
		double test = 0;
		sscanf(token, "%d", &label);
		if (label != 3 && label != 8) continue;
		if (label == 3) {
			label = 1;
		}
		else {
			label = -1;
		}
		token = strtok(NULL, " \n");
		while (token) {
			sscanf(token, "%d:%lf", &key, &value);
			token = strtok(NULL, " \n");
			assert(key >= 1 && key <= n_feature);
			test += value * x[key - 1];
			norm += value * value;
		}
		norm = sqrt(norm);
		test /= norm;
		test += x[n_feature];
		test *= label;
		if (test > 0) correct++;
		total++;
	}
	printf("%d %d %f\n", correct, total, correct * 1. / total);
	fclose(f);
	return 0;
}



int a9a() {
	int n_sample = 32561;
	int n_feature = 123;
	int n_avg = 20;

	char line[10000];
	FILE *f = fopen("../Data/a9a", "r");

	
	int m = 0;
	int n = n_feature + 1 + n_sample;
	double *x = (double*)malloc(sizeof(double) * n);
	double y;
	int i, j;
	double *r = (double*)malloc(sizeof(double) * n_sample);
	int *c_rowptr = (int*)malloc(sizeof(int) * (n_sample + 1));
	int *c_col = (int*)malloc(sizeof(int) * n_avg * n);
	double *c_val = (double*)malloc(sizeof(double) * n_avg * n);
	int correct = 0;
	int total = 0;

	memset(r, 0, sizeof(double) * n_sample);
	memset(x, 0, sizeof(double) * n);
	c_rowptr[0] = 0;
	
	while (fgets(line, 10000, f)) {
		int label;
		char *token = strtok(line, " \n");
		int key;
		double value;
		double norm = 0;
		sscanf(token, "%d", &label);

		c_rowptr[m + 1] = c_rowptr[m];
		token = strtok(NULL, " \n");
		while (token) {
			sscanf(token, "%d:%lf", &key, &value);
			token = strtok(NULL, " \n");
			assert(key >= 1 && key <= n_feature);
			c_val[c_rowptr[m + 1]] = label * value;
			c_col[c_rowptr[m + 1]] = key - 1;
			norm += value * value;
			c_rowptr[m + 1]++;
		}
		norm = sqrt(norm);
		for (i = c_rowptr[m]; i < c_rowptr[m + 1]; i++) {
			c_val[i] /= norm;
		}
		c_val[c_rowptr[m + 1]] = label;
		c_col[c_rowptr[m + 1]] = n_feature;
		c_rowptr[m + 1]++;
		c_val[c_rowptr[m + 1]] = 1. / sqrt(P);
		c_col[c_rowptr[m + 1]] = n_feature + 1 + m;
		c_rowptr[m + 1]++;
		m++;
		
	}
	
	assert(m == n_sample);
	fclose(f);
	
	for (i = 0; i < n; i++) {
		x[i] = 0;
	}

	y = truncate_newton_cg(m, n, x, r, c_rowptr, c_col, c_val);

	for (i = 0; i < m; i++) {
		double test = 0;
		for (j = c_rowptr[i]; j < c_rowptr[i + 1]; j++) {
			if (c_col[i] > n_feature) continue;
			test += c_val[j] * x[c_col[j]];
		}
		if (test > 0) correct++;
		total++;
	}
	printf("%d %d %f\n", correct, total, correct * 1. / total);

	correct = total = 0;
	f = fopen("../Data/a9a.t", "r");
	while (fgets(line, 10000, f)) {
		int label;
		char *token = strtok(line, " \n");
		int key;
		double value;
		double norm = 0;
		double test = 0;
		sscanf(token, "%d", &label);
		token = strtok(NULL, " \n");
		while (token) {
			sscanf(token, "%d:%lf", &key, &value);
			token = strtok(NULL, " \n");
			assert(key >= 1 && key <= n_feature);
			test += value * x[key - 1];
			norm += value * value;
		}
		norm = sqrt(norm);
		test /= norm;
		test += x[n_feature];
		test *= label;
		if (test > 0) correct++;
		total++;
	}
	printf("%d %d %f\n", correct, total, correct * 1. / total);
	fclose(f);
	return 0;
}
/*
int ijcnn1() {
	int n_sample = 49990;
	int n_feature = 22;
	int n_avg = 20;

	char line[10000];
	FILE *f = fopen("../Data/ijcnn1", "r");

	
	int m = 0;
	int n = n_feature + 1 + n_sample;
	double *x = (double*)malloc(sizeof(double) * n);
	double y;
	int i, j;
	double *r = (double*)malloc(sizeof(double) * n_sample);
	int *c_rowptr = (int*)malloc(sizeof(int) * (n_sample + 1));
	int *c_col = (int*)malloc(sizeof(int) * n_avg * n);
	double *c_val = (double*)malloc(sizeof(double) * n_avg * n);
	int correct = 0;
	int total = 0;

	memset(r, 0, sizeof(double) * n_sample);
	memset(x, 0, sizeof(double) * n);
	c_rowptr[0] = 0;
	
	while (fgets(line, 10000, f)) {
		int label;
		char *token = strtok(line, " \n");
		int key;
		double value;
		double norm = 0;
		sscanf(token, "%d", &label);

		c_rowptr[m + 1] = c_rowptr[m];
		token = strtok(NULL, " \n");
		while (token) {
			sscanf(token, "%d:%lf", &key, &value);
			token = strtok(NULL, " \n");
			assert(key >= 1 && key <= n_feature);
			c_val[c_rowptr[m + 1]] = label * value;
			c_col[c_rowptr[m + 1]] = key - 1;
			norm += value * value;
			c_rowptr[m + 1]++;
		}
		norm = sqrt(norm);
		for (i = c_rowptr[m]; i < c_rowptr[m + 1]; i++) {
			c_val[i] /= norm;
		}
		c_val[c_rowptr[m + 1]] = label;
		c_col[c_rowptr[m + 1]] = n_feature;
		c_rowptr[m + 1]++;
		c_val[c_rowptr[m + 1]] = 1. / sqrt(P);
		c_col[c_rowptr[m + 1]] = n_feature + 1 + m;
		c_rowptr[m + 1]++;
		m++;
		
	}
	
	assert(m == n_sample);
	fclose(f);
	
	for (i = 0; i < n; i++) {
		x[i] = 0;
	}

	y = truncate_newton_cg(m, n, x, r, c_rowptr, c_col, c_val);

	for (i = 0; i < m; i++) {
		double test = 0;
		for (j = c_rowptr[i]; j < c_rowptr[i + 1]; j++) {
			if (c_col[i] > n_feature) continue;
			test += c_val[j] * x[c_col[j]];
		}
		if (test > 0) correct++;
		total++;
	}
	printf("%d %d %f\n", correct, total, correct * 1. / total);

	correct = total = 0;
	f = fopen("../Data/ijcnn1.t", "r");
	while (fgets(line, 10000, f)) {
		int label;
		char *token = strtok(line, " \n");
		int key;
		double value;
		double norm = 0;
		double test = 0;
		sscanf(token, "%d", &label);
		token = strtok(NULL, " \n");
		while (token) {
			sscanf(token, "%d:%lf", &key, &value);
			token = strtok(NULL, " \n");
			assert(key >= 1 && key <= n_feature);
			test += value * x[key - 1];
			norm += value * value;
		}
		norm = sqrt(norm);
		test /= norm;
		test += x[n_feature];
		test *= label;
		if (test > 0) correct++;
		total++;
	}
	printf("%d %d %f\n", correct, total, correct * 1. / total);
	fclose(f);
	return 0;
}
*/
int ijcnn1() {
	int n_sample = 49990;
	int n_feature = 22;
	int n_avg = 100;

	char line[10000];
	FILE *f = fopen("../Data/ijcnn1", "r");

	
	int m = 0;
	int n = n_feature + 1;
	double *x = (double*)malloc(sizeof(double) * n);
	double y;
	int i, j;
	double *r = (double*)malloc(sizeof(double) * n_sample);
	int *c_rowptr = (int*)malloc(sizeof(int) * (n_sample + 1));
	int *c_col = (int*)malloc(sizeof(int) * n_avg * n_sample);
	double *c_val = (double*)malloc(sizeof(double) * n_avg * n_sample);
	int correct = 0;
	int total = 0;

	memset(r, 0, sizeof(double) * n_sample);
	memset(x, 0, sizeof(double) * n);
	c_rowptr[0] = 0;
	
	while (fgets(line, 10000, f)) {
		int label;
		char *token = strtok(line, " \n");
		int key;
		double value;
		double norm = 0;
		sscanf(token, "%d", &label);

		c_rowptr[m + 1] = c_rowptr[m];
		token = strtok(NULL, " \n");
		while (token) {
			sscanf(token, "%d:%lf", &key, &value);
			token = strtok(NULL, " \n");
			assert(key >= 1 && key <= n_feature);
			c_val[c_rowptr[m + 1]] = P * label * value;
			c_col[c_rowptr[m + 1]] = key - 1;
			norm += value * value;
			c_rowptr[m + 1]++;
		}
		
		//norm = sqrt(norm);
		/*
		for (i = c_rowptr[m]; i < c_rowptr[m + 1]; i++) {
			c_val[i] /= norm;
		}
		*/
	//	r[m] = sqrt(norm);
		
		c_val[c_rowptr[m + 1]] = P * label;
		c_col[c_rowptr[m + 1]] = n_feature;
		c_rowptr[m + 1]++;
/*
		c_val[c_rowptr[m + 1]] = 1. / sqrt(P);
		c_col[c_rowptr[m + 1]] = n_feature + 1 + m;
		c_rowptr[m + 1]++;
*/
		m++;
		
	}
	
	assert(m == n_sample);
	fclose(f);
	
	for (i = 0; i < n; i++) {
		x[i] = 0;
	}

	y = truncate_newton_cg(m, n, x, r, c_rowptr, c_col, c_val);

	for (i = 0; i < m; i++) {
		double test = 0;
		for (j = c_rowptr[i]; j < c_rowptr[i + 1]; j++) {
			if (c_col[i] > n_feature) continue;
			test += c_val[j] * x[c_col[j]];
		}
		if (test > 0) correct++;
		total++;
	}
	printf("%d %d %f\n", correct, total, correct * 1. / total);

	correct = total = 0;
	f = fopen("../Data/ijcnn1.t", "r");
	while (fgets(line, 10000, f)) {
		int label;
		char *token = strtok(line, " \n");
		int key;
		double value;
		double norm = 0;
		double test = 0;
		sscanf(token, "%d", &label);
		token = strtok(NULL, " \n");
		while (token) {
			sscanf(token, "%d:%lf", &key, &value);
			token = strtok(NULL, " \n");
			assert(key >= 1 && key <= n_feature);
			test += value * x[key - 1];
			norm += value * value;
		}
		norm = sqrt(norm);
		//test /= norm;
		test += x[n_feature];
		test *= label;
		if (test > 0) correct++;
		total++;
	}
	printf("%d %d %f\n", correct, total, correct * 1. / total);
	fclose(f);
	return 0;
}

int realsim() {
	int n_sample = 72309;
	int n_feature = 25000;
	int n_avg = 10000;

	char line[1000000];
	FILE *f = fopen("../Data/real-sim", "r");
	
	int m = 0;
	int n = n_feature + 1 + n_sample;
	double *x = (double*)malloc(sizeof(double) * n);
	double y;
	int i, j;
	double *r = (double*)malloc(sizeof(double) * n_sample);
	int *c_rowptr = (int*)malloc(sizeof(int) * (n_sample + 1));
	int *c_col = (int*)malloc(sizeof(int) * n_avg * n);
	double *c_val = (double*)malloc(sizeof(double) * n_avg * n);
	int correct = 0;
	int total = 0;

	memset(r, 0, sizeof(double) * n_sample);
	memset(x, 0, sizeof(double) * n);
	c_rowptr[0] = 0;
	
	while (fgets(line, 1000000, f)) {
		int label;
		char *token = strtok(line, " \n");
		int key;
		double value;
		double norm = 0;
		if (1 != sscanf(token, "%d", &label)) {
			fprintf(stderr, "Error\n");
			printf("line %d\n", m + 1);
			return 0;
		}

		c_rowptr[m + 1] = c_rowptr[m];
		token = strtok(NULL, " \n");
		while (token) {
			if (2 != sscanf(token, "%d:%lf", &key, &value)) {
				fprintf(stderr, "Error\n");
				printf("line %d\n", m + 1);
				return 0;
			}
			token = strtok(NULL, " \n");
			assert(key >= 1 && key <= n_feature);
			c_val[c_rowptr[m + 1]] = label * value;
			c_col[c_rowptr[m + 1]] = key - 1;
			norm += value * value;
			c_rowptr[m + 1]++;
		}
		norm = sqrt(norm);
		for (i = c_rowptr[m]; i < c_rowptr[m + 1]; i++) {
			c_val[i] /= norm;
		}
		c_val[c_rowptr[m + 1]] = label;
		c_col[c_rowptr[m + 1]] = n_feature;
		c_rowptr[m + 1]++;
		c_val[c_rowptr[m + 1]] = 1. / sqrt(P);
		c_col[c_rowptr[m + 1]] = n_feature + 1 + m;
		c_rowptr[m + 1]++;
		m++;
		
	}
	printf("m = %d\n", m);
	assert(m == n_sample);
	fclose(f);
	
	for (i = 0; i < n; i++) {
		x[i] = 0;
	}

	y = truncate_newton_cg(m, n, x, r, c_rowptr, c_col, c_val);

	for (i = 0; i < m; i++) {
		double test = 0;
		for (j = c_rowptr[i]; j < c_rowptr[i + 1]; j++) {
			if (c_col[i] > n_feature) continue;
			test += c_val[j] * x[c_col[j]];
		}
		if (test > 0) correct++;
		total++;
	}
	printf("%d %d %f\n", correct, total, correct * 1. / total);

	return 0;
	correct = total = 0;
	f = fopen("../Data/ijcnn1.t", "r");
	while (fgets(line, 1000000, f)) {
		int label;
		char *token = strtok(line, " \n");
		int key;
		double value;
		double norm = 0;
		double test = 0;
		sscanf(token, "%d", &label);
		token = strtok(NULL, " \n");
		while (token) {
			sscanf(token, "%d:%lf", &key, &value);
			token = strtok(NULL, " \n");
			assert(key >= 1 && key <= n_feature);
			test += value * x[key - 1];
			norm += value * value;
		}
		norm = sqrt(norm);
		test /= norm;
		test += x[n_feature];
		test *= label;
		if (test > 0) correct++;
		total++;
	}
	printf("%d %d %f\n", correct, total, correct * 1. / total);
	fclose(f);
	return 0;
}

int gisette() {
	int n_sample = 6000;
	int n_feature = 5000;
	int n_avg = 5000;

	char line[1000000];
	FILE *f = fopen("../Data/gisette_scale", "r");
	
	int m = 0;
	int n = n_feature + 1 + n_sample;
	double *x = (double*)malloc(sizeof(double) * n);
	double y;
	int i, j;
	double *r = (double*)malloc(sizeof(double) * n_sample);
	int *c_rowptr = (int*)malloc(sizeof(int) * (n_sample + 1));
	int *c_col = (int*)malloc(sizeof(int) * n_avg * n);
	double *c_val = (double*)malloc(sizeof(double) * n_avg * n);
	int correct = 0;
	int total = 0;

	memset(r, 0, sizeof(double) * n_sample);
	memset(x, 0, sizeof(double) * n);
	c_rowptr[0] = 0;
	
	while (fgets(line, 1000000, f)) {
		int label;
		char *token = strtok(line, " \n");
		int key;
		double value;
		double norm = 0;
		if (1 != sscanf(token, "%d", &label)) {
			fprintf(stderr, "Error\n");
			printf("line %d\n", m + 1);
			return 0;
		}

		c_rowptr[m + 1] = c_rowptr[m];
		token = strtok(NULL, " \n");
		while (token) {
			if (2 != sscanf(token, "%d:%lf", &key, &value)) {
				fprintf(stderr, "Error\n");
				printf("line %d\n", m + 1);
				return 0;
			}
			token = strtok(NULL, " \n");
			assert(key >= 1 && key <= n_feature);
			c_val[c_rowptr[m + 1]] = label * value;
			c_col[c_rowptr[m + 1]] = key - 1;
			norm += value * value;
			c_rowptr[m + 1]++;
		}
		norm = sqrt(norm);
		for (i = c_rowptr[m]; i < c_rowptr[m + 1]; i++) {
			c_val[i] /= norm;
		}
		c_val[c_rowptr[m + 1]] = label;
		c_col[c_rowptr[m + 1]] = n_feature;
		c_rowptr[m + 1]++;
		c_val[c_rowptr[m + 1]] = 1. / sqrt(P);
		c_col[c_rowptr[m + 1]] = n_feature + 1 + m;
		c_rowptr[m + 1]++;
		m++;
		
	}
	printf("m = %d\n", m);
	assert(m == n_sample);
	fclose(f);
	
	for (i = 0; i < n; i++) {
		x[i] = 0;
	}

	y = truncate_newton_cg(m, n, x, r, c_rowptr, c_col, c_val);

	for (i = 0; i < m; i++) {
		double test = 0;
		for (j = c_rowptr[i]; j < c_rowptr[i + 1]; j++) {
			if (c_col[i] > n_feature) continue;
			test += c_val[j] * x[c_col[j]];
		}
		if (test > 0) correct++;
		total++;
	}
	printf("%d %d %f\n", correct, total, correct * 1. / total);

	return 0;
	correct = total = 0;
	f = fopen("../Data/ijcnn1.t", "r");
	while (fgets(line, 1000000, f)) {
		int label;
		char *token = strtok(line, " \n");
		int key;
		double value;
		double norm = 0;
		double test = 0;
		sscanf(token, "%d", &label);
		token = strtok(NULL, " \n");
		while (token) {
			sscanf(token, "%d:%lf", &key, &value);
			token = strtok(NULL, " \n");
			assert(key >= 1 && key <= n_feature);
			test += value * x[key - 1];
			norm += value * value;
		}
		norm = sqrt(norm);
		test /= norm;
		test += x[n_feature];
		test *= label;
		if (test > 0) correct++;
		total++;
	}
	printf("%d %d %f\n", correct, total, correct * 1. / total);
	fclose(f);
	return 0;
}


int main() {
	//mnist38();
	//a9a();
	ijcnn1();
	//realsim();
	//gisette();
	return 0;
}
