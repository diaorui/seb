#include <stdio.h>
#include "la.h"
#include <memory.h>
#include <malloc.h>
#include <math.h>
#include <stdio.h>
#include "util.h"
#include "alloc.h"
#include "cg.h"
#include "line_search.h"
#include "truncate_newton_cg.h"
#include <time.h>

double truncate_newton_cg(const int m, const int n, double *x, double *r, double *C) {
	int mem_size = sizeof(double) * (m * 5 + 6 * n) + sizeof(int) * m;
	void *mem = malloc(mem_size);
	const double e1 = 1e-6;
	const double e3 = 1e-2;
	const double sigma = 0.1;
	double mu = 1;
	clock_t t1, t2;
	double s_bound;
	double timing;
	double y;

	while (mu >= e1) {
      printf("mu = %f\n", mu);

	    t1 = clock();
		s_bound = mu * log(e3 * mu / 10);
		truncate_newton_cg_sub(x, mu, r, C, m, n, s_bound, mem);
		mu *= sigma;
	    
        t2 = clock();	
	    timing = (double) (t2 - t1) / (double) CLOCKS_PER_SEC;
	    printf("time = %.10e\n", timing);
	}
	
	y = calc_f(n, m, x, C, r);

	free(mem);

	return y;	
}


void truncate_newton_cg_sub(double *x, double mu, double *r, double *C, const int m, const int n, double s_bound, void *memalloc) {
	double c1 = 1e-4;
	//double c2 = 0.99;
	double e2;
	double norm_tilde_grad_f;
	int size_s;

	double *g = (double *) alloc(&memalloc, sizeof(double) * m);
	double *f_comp = (double *) alloc(&memalloc, sizeof(double) * m);
	double *exp_f = (double *) alloc(&memalloc, sizeof(double) * m);
	double *lambda = (double *) alloc(&memalloc, sizeof(double) * m);
	int *s = (int *) alloc(&memalloc, sizeof(int) * m);
	double *tilde_grad_f = (double *) alloc(&memalloc, sizeof(double) * n);
	double *direction = (double *) alloc(&memalloc, sizeof(double) * n);
	double *x_new = (double *) alloc(&memalloc, sizeof(double) * n);
	double f_new;
	double max_f_comp;
	double sum_exp_f_s;
	double sum_exp_f;
	double f;
	int iter = 0;

	e2 = mu * 0.1;
	e2 = e2 < 1e-5 ? 1e-5 : e2;
	e2 = e2 > 1e-1 ? 1e-1 : e2;
//	e2 = 1e-5;

	e2 = e2 * e2;
	calc_g(n, m, x, C, mu * mu, g);
	calc_f_comp(m, g, r, f_comp);
	max_f_comp = max_val(m, f_comp);
	calc_s(m, max_f_comp, f_comp, s_bound, &size_s, s);
  fprintf(stderr, "size_s = %d\n", size_s);
	calc_exp_f(m, max_f_comp, f_comp, mu, exp_f);
	sum_exp_f_s = sumup_s(size_s, s, exp_f);
	sum_exp_f = sumup(m, exp_f);
	f = max_f_comp + mu * log(sum_exp_f);
	calc_lambda(size_s, s, exp_f, sum_exp_f_s, lambda);
	calc_tilde_grad_f(n, size_s, s, g, x, C, lambda, tilde_grad_f);
	
	norm_tilde_grad_f = inner_product(n, tilde_grad_f, tilde_grad_f);
//	double max_abs_tilde_grad_f = max_abs(n, tilde_grad_f);

	while (norm_tilde_grad_f > e2) {
        iter++;
        //printf("iter = %d, size_s = %d\n", iter, size_s);
//	while (max_abs_tilde_grad_f > e2) {
		cg(n, size_s, s, C, x, g, mu, lambda, tilde_grad_f, norm_tilde_grad_f, memalloc, direction);
	
		line_search(n, m, x, direction, tilde_grad_f, C, mu, g, r, f_comp, &max_f_comp, exp_f, c1, f, &f_new, x_new);
		
		calc_s(m, max_f_comp, f_comp, s_bound, &size_s, s);
    fprintf(stderr, "size_s = %d\n", size_s);
		sum_exp_f_s = sumup_s(size_s, s, exp_f);
		calc_lambda(size_s, s, exp_f, sum_exp_f_s, lambda);
		calc_tilde_grad_f(n, size_s, s, g, x_new, C, lambda, tilde_grad_f);

		memcpy(x, x_new, sizeof(double) * n);

		f = f_new;
		
		norm_tilde_grad_f = inner_product(n, tilde_grad_f, tilde_grad_f);
//		max_abs_tilde_grad_f = max_abs(n, tilde_grad_f);
	}
}

