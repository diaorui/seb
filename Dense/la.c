#include <math.h>

double inner_product(int n, double *a, double *b) {
	int i;
	double res = 0;

	for (i = 0; i < n; i++) {
		res += a[i] * b[i];
	}

	return res;
}

double max_val(const int n, double *v) {
	int i;
	double res = v[0];

	for (i = 1; i < n; i++) {
		if (res < v[i]) {
			res = v[i];
		}
	}

	return res;
}

double max_abs(const int n, double *v) {
	int i;
	double res = fabs(v[0]);

	for (i = 1; i < n; i++) {
		if (res < fabs(v[i])) {
			res = fabs(v[i]);
		}
	}

	return res;
}

double sumup(const int n, double *v) {
	int i;
	double res = 0;
	for (i = 0; i < n; i++) {
		res += v[i];
	}
	return res;
}

double sumup_s(const int n, int *s, double *v) {
	int i;
	double res = 0;
	for (i = 0; i < n; i++) {
		res += v[s[i]];
	}
	return res;
}

