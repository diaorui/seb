#include <stdio.h>
#include "la.h"
#include <memory.h>
#include <malloc.h>
#include <math.h>
#include <stdio.h>
#include "util.h"
#include "alloc.h"
#include "cg.h"

void cg(const int n, const int size_s, int *s, double *C, double *x, double *g, double mu, double *lambda, double *tilde_grad_f, const double norm_tilde_grad_f, void *memalloc, double *direction) {

	int i;
	double residual_bound = calc_residual_bound(n, norm_tilde_grad_f);
	double residual_norm = norm_tilde_grad_f;

	double *residual = (double *) alloc(&memalloc, sizeof(double) * n);
	double *hessian_coeff1 = (double *) alloc(&memalloc, sizeof(double) * size_s);
	double hessian_coeff2;
	double *p = (double *) alloc(&memalloc, sizeof(double) * n);
	double *ap = (double *) alloc(&memalloc, sizeof(double) * n);
	double pap;
	double alpha;
	double residual_norm_new;
	double beta;

	for (i = 0; i < n; i++) {
		residual[i] = - tilde_grad_f[i];
	}
	memset(direction, 0, sizeof(direction[0]) * n);
	memcpy(p, residual, sizeof(p[0]) * n);
	calc_hessian_coeff(size_s, s, g, mu, lambda, hessian_coeff1, &hessian_coeff2);

	while (residual_norm > residual_bound) {
		hessian_mul_vector(n, size_s, s, mu, tilde_grad_f, C, x, p, hessian_coeff1, hessian_coeff2, ap);
		pap = inner_product(n, p, ap);
		alpha = residual_norm / pap;
		for (i = 0; i < n; i++) {
			direction[i] += alpha * p[i];
		}
		for (i = 0; i < n; i++) {
			residual[i] -= alpha * ap[i];
		}
		residual_norm_new = inner_product(n, residual, residual);
		beta = residual_norm_new / residual_norm;
		residual_norm = residual_norm_new;
		for (i = 0; i < n; i++) {
			p[i] = residual[i] + beta * p[i];
		}
	}
}

