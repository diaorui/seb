#include "la.h"
#include <math.h>
#include "alloc.h"
#include "util.h"
#include <memory.h>
#include <stdio.h>


void calc_g(const int n, const int m, double *x, double *C, const double mu2, double *g) {
	int i, j;

	for (i = 0; i < m; i++) {
		double sum = 0;
		int offset = i * n;
		for (j = 0; j < n; j++, offset++) {
			double tmp = x[j] - C[offset];
			sum += tmp * tmp;
		}
		g[i] = sqrt(sum + mu2);
	}
}

void calc_f_comp(const int m, double *g, double *r, double *f_comp) {
	int i;
	for (i = 0; i < m; i++) {
		f_comp[i] = g[i] + r[i];
	}
}

void calc_s(const int m, const double max_f_comp, double *f_comp, const double bound, int *size_s, int *s) {
	int i;

	*size_s = 0;
	
	for (i = 0; i < m; i++) {
		if ((f_comp[i] - max_f_comp) > bound) {
			s[(*size_s)++] = i;
		}
	}
}

void calc_exp_f(const int m, const double max_f_comp, double *f_comp, const double mu, double *exp_f) {
	int i;
	for (i = 0; i < m; i++) {
		exp_f[i] = exp((f_comp[i] - max_f_comp) / mu);
	}
}

void calc_exp_f_s(const int size_s, int *s, const double max_f_comp, double *f_comp, const double mu, double *exp_f) {
	int i;
	for (i = 0; i < size_s; i++) {
		exp_f[s[i]] = exp((f_comp[s[i]] - max_f_comp) / mu);
	}
}

void calc_lambda(const int size_s, int *s, double *exp_f, const double sum_exp_f_s, double *lambda) {
	int i;
	for (i = 0; i < size_s; i++) {
		lambda[i] = exp_f[s[i]] / sum_exp_f_s;
	}
}

void calc_tilde_grad_f(const int n, const int size_s, int *s, double *g, double *x, double *C, double *lambda, double *tilde_grad_f) {
	int i, j;

	memset(tilde_grad_f, 0, sizeof(tilde_grad_f[0]) * n);

	for (i = 0; i < size_s; i++) {
		int offset = s[i] * n;
		for (j = 0; j < n; j++, offset++) {
			tilde_grad_f[j] += (x[j] - C[offset]) / g[s[i]] * lambda[i];
		}	
	}
}

double calc_f(const int n, const int m, double *x, double *C, double *r) {
	int i, j;
	double res = 0;
	for (i = 0; i < m; i++) {
		double val = 0;
		int offset = i * n;
		for (j = 0; j < n; j++, offset++) {
			double tmp = x[j] - C[offset];
			val += tmp * tmp;
		}
		val = sqrt(val) + r[i];
		if (res < val) {
			res = val;
		}
	}
	return res;
}

void calc_hessian_coeff(const int size_s, int *s, double *g, const double mu, double *lambda, double *hessian_coeff1, double *hessian_coeff2) {
	int i;

	*hessian_coeff2 = 0;
	for (i = 0; i < size_s; i++) {
		double tmp = lambda[i] / g[s[i]];
		*hessian_coeff2 += tmp;
		hessian_coeff1[i] = (1. / mu - 1. / g[s[i]]) * tmp / g[s[i]];
	}
}

double calc_residual_bound(const int n, const double norm) {
	double sqrt_norm = sqrt(norm);
	if (sqrt_norm < 0.25) {
		return sqrt_norm * norm;
	}
	else {
		return 0.25 * norm;
	}
}

void hessian_mul_vector(const int n, const int size_s, int *s, double mu, double *tilde_grad_f, double *C, double *x, double *v, double* hessian_coeff1, const double hessian_coeff2, double *hessian_v) {
	int i, j;
	double tmp;
	int offset;

    tmp = 0;
	for (j = 0; j < n; j++) {
		tmp += tilde_grad_f[j] * v[j];
	}
	tmp /= - mu;
	for (j = 0; j < n; j++) {
		hessian_v[j] = tmp * tilde_grad_f[j];
	}

    for (i = 0; i < size_s; i++) {
		tmp = 0;
		offset = s[i] * n;
		for (j = 0; j < n; j++, offset++) {
			tmp += (x[j] - C[offset]) * v[j];
		}

		tmp *= hessian_coeff1[i];
		offset -= n;
		for (j = 0; j < n; j++, offset++) {
			hessian_v[j] += tmp * (x[j] - C[offset]);
		}
	}
	
    for (j = 0; j < n; j++) {
		hessian_v[j] += hessian_coeff2 * v[j];
	}
}

