#include "line_search.h"
#include "util.h"

void line_search(const int n, const int m, double *x, double *direction, double *tilde_grad_f, double *C, const double mu, double *g, double *r, double *f_comp, double *max_f_comp, double *exp_f, const double c1, const double f, double *f_new, double *x_new) {
	int i;

	double step_size = 1;
	double d_tilde_grad_f = inner_product(n, direction, tilde_grad_f);
	double sum_exp_f;

	while (1) {
		for (i = 0; i < n; i++) {
			x_new[i] = x[i] + step_size * direction[i];
		}

		calc_g(n, m, x_new, C, mu * mu, g);
		calc_f_comp(m, g, r, f_comp);
		*max_f_comp = max_val(m, f_comp);
		calc_exp_f(m, *max_f_comp, f_comp, mu, exp_f);
		sum_exp_f = sumup(m, exp_f);
		*f_new = *max_f_comp + mu * log(sum_exp_f);
    
		if (*f_new - f <= c1 * step_size * d_tilde_grad_f) {
			break;
		}
		step_size *= 0.5;
	}
}

