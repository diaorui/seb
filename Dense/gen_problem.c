#include <memory.h>
#include <malloc.h>
#include "gen_problem.h"

// m is the number of balls
// n is the dimension of balls
void gen_problem(int *m, int *n, double **r, double **C) {

	int k = 0;
	int *fai = (int *) malloc(sizeof(int) * (*n + 2));
	int i;

//	*m = 1000;
//	*n = 100;
	*r = (double *) malloc(sizeof(double) * *m);
	*C = (double *) malloc(sizeof(double) * *m * *n);
//	*normC = (double *) malloc(sizeof(double) * *m);



	fai[0] = 7;

	while (k < *m) {
		for (i = 0; i <= *n; i++) {
			fai[i + 1] = (445 * fai[i] + 1) % 4096;
		}

		(*r)[k] = fai[1] / 40.96;

		//(*normC)[k] = 0;
		for (i = 0; i < *n; i++) {
			(*C)[i + k * *n] = fai[2 + i] / 40.96;
			//(*normC)[k] += (*C)[i + k * *n] * (*C)[i + k * *n];
		}

		k++;
		fai[0] = fai[*n + 1];        
	}

	free(fai);
}

