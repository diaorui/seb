#ifndef __ALLOC_H__
#define __ALLOC_H__

void* alloc(void **pool, int size);

#endif
