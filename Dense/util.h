#ifndef __UTIL_H__
#define __UTIL_H__

#include "la.h"
#include <math.h>
#include "alloc.h"

void calc_g(const int n, const int m, double *x, double *C, const double mu2, double *g);
void calc_f_comp(const int m, double *g, double *r, double *f_comp);
void calc_s(const int m, const double max_f_comp, double *f_comp, const double bound, int *size_s, int *s);
void calc_exp_f(const int m, const double max_f_comp, double *f_comp, const double mu, double *exp_f);
void calc_exp_f_s(const int size_s, int *s, const double max_f_comp, double *f_comp, const double mu, double *exp_f);
void calc_lambda(const int size_s, int *s, double *exp_f, const double sum_exp_f_s, double *lambda);
void calc_tilde_grad_f(const int n, const int size_s, int *s, double *g, double *x, double *C, double *lambda, double *tilde_grad_f);
double calc_f(const int n, const int m, double *x, double *C, double *r);
void calc_hessian_coeff(const int size_s, int *s, double *g, const double mu, double *lambda, double *hessian_coeff1, double *hessian_coeff2);
double calc_residual_bound(const int n, const double norm);
void hessian_mul_vector(const int n, const int size_s, int *s, double mu, double *tilde_grad_f, double *C, double *x, double *v, double *hessian_coeff1, const double hessian_coeff2, double *hessian_v);

#endif
