#include <sys/time.h>
#include <stdio.h>
#include <malloc.h>
#include <memory.h>
#include <math.h>
#include <time.h>
#include "gen_problem.h"
#include "truncate_newton_cg.h"
#include "util.h"
#include <assert.h>
#include <string.h>
#include "f2c.h"
/*
P=5
11697 11982 0.976214
1926 1984 0.970766
*/
    extern /* Subroutine */ int setulb_(integer *, integer *, doublereal *, 
	    doublereal *, doublereal *, integer *, doublereal *, doublereal *,
	     doublereal *, doublereal *, doublereal *, integer *, char *, 
	    integer *, char *, logical *, integer *, doublereal *, ftnlen, 
	    ftnlen);

void run_test(int m, int n) {
	clock_t t1 = clock(), t2;

	double *r, *C;
	int i;
	double *x;
	double y;
	double timing;

	gen_problem(&m, &n, &r, &C);

	x = (double*)malloc(sizeof(double) * n);
	for (i = 0; i < n; i++) {
		x[i] = 0;
	}

	y = truncate_newton_cg(m, n, x, r, C);

	free(x);
	free(C);
	free(r);
	
	t2 = clock();	
	timing = (double) (t2 - t1) / (double) CLOCKS_PER_SEC;
	printf("%8d%8d  %.10e  %.10e\n", m, n, timing, y);

}
/*
int mlist1[] = {1000,1000,1000,2000,3000,4000,5000,6000,7000,8000,9000,10000,20000};
int nlist1[] = {400,800,1000,1000,1000,1000,1000,1000,1000,1000,1000,1000,1000};

int mlist2[] = {8000,16000,32000,64000,128000,256000,512000};
int nlist2[] = {100,100,100,100,100,100,100};

int mlist3[] = {1000,2000,3000,4000,5000,6000,7000,8000,9000,10000};
int nlist3[] = {2000,2000,2000,2000,2000,2000,2000,2000,2000,2000};

int mlist4[] = {2000,2000,2000,2000,2000,2000,2000,2000,5000,5000,5000,5000,5000,5000,5000,5000,8000,10000,10000};
int nlist4[] = {3000,4000,5000,6000,7000,8000,9000,10000,3000,4000,5000,6000,7000,8000,9000,10000,7000,8000,10000};

int mlist5[] = {30000, 40000, 50000, 100000, 1024000, 2048000, 20000, 30000, 40000, 50000, 100000, 20000};
int nlist5[] = {1000, 1000, 1000, 1000, 100, 100, 2000, 2000, 2000, 2000, 2000, 10000};


int main() {

	int i;

	printf("%8s%8s%12s%12s\n", "m", "n", "cpu", "y");
	for (i = 0; i < sizeof(mlist1) / sizeof(int); i++) {
		run_test(mlist1[i],nlist1[i]); 
	}

	printf("%8s%8s%12s%12s\n", "m", "n", "cpu", "y");
	for (i = 0; i < sizeof(mlist2) / sizeof(int); i++) {
		run_test(mlist2[i],nlist2[i]); 
	}
	
	printf("%8s%8s%12s%12s\n", "m", "n", "cpu", "y");
	for (i = 0; i < sizeof(mlist3) / sizeof(int); i++) {
		run_test(mlist3[i],nlist3[i]); 
	}
	printf("%8s%8s%12s%12s\n", "m", "n", "cpu", "y");
	for (i = 0; i < sizeof(mlist4) / sizeof(int); i++) {
		run_test(mlist4[i],nlist4[i]); 
	}
	printf("%8s%8s%12s%12s\n", "m", "n", "cpu", "y");
	for (i = 0; i < sizeof(mlist5) / sizeof(int); i++) {
		run_test(mlist5[i],nlist5[i]); 
	}
	return 0;
}

*/

void test_bls(const char *filename) {
	FILE *f = fopen(filename, "r");
	char line[10000];
	int m, n;
	int i = 0, j;
	double *x = NULL;
	double *r = NULL;
	double *C = NULL;
	double y;
	char *token;

	fgets(line, 10000, f);
	printf("Test File : %s", line);
	
	while (fgets(line, 10000, f)) {
		if (line[0] == '#') continue;
		if (strncmp(line, "d ", 2) == 0) {
			sscanf(line, "d %d", &n);
			continue;
		}
		if (strncmp(line, "n ", 2) == 0) {
			sscanf(line, "n %d", &m);	
			continue;
		}
		if (NULL == x) {
			x = (double *)malloc(sizeof(double) * n);
			memset(x, 0, sizeof(double) * n);
			r = (double *)malloc(sizeof(double) * m);
			C = (double *)malloc(sizeof(double) * m * n);
		}
		token = strtok(line, " \n");
		for (j = 0; j < n; j++) {
			sscanf(token, "%lf", &C[i * n + j]);
			token = strtok(NULL, " \n");
		}
		sscanf(token, "%lf", &r[i]);
    i++;
	}
	if (x) {
	  clock_t t1 = clock(), t2;
    double timing;
		y = truncate_newton_cg(m, n, x, r, C);
    t2 = clock();
	  timing = (double) (t2 - t1) / (double) CLOCKS_PER_SEC;
	  printf("%8d%8d  %.10e  %.10e\n", m, n, timing, y);

		for (i = 0; i < n; i++) {
			printf("%.6f\n", x[i]);
		}
		printf("y = %.6f\n", y);
		free(x);
		free(r);
		free(C);
	}
	fclose(f);
}

void test_bls_with_lbfgs(const char *filename) {
	FILE *f = fopen(filename, "r");
	char line[10000];
	int m, n;
	int i = 0, j;
	double *x = NULL;
	double *r = NULL;
	double *C = NULL;
  double *C_;
	double y;
	char *token;

	fgets(line, 10000, f);
	printf("Test File : %s", line);
	
	while (fgets(line, 10000, f)) {
		if (line[0] == '#') continue;
		if (strncmp(line, "d ", 2) == 0) {
			sscanf(line, "d %d", &n);
			continue;
		}
		if (strncmp(line, "n ", 2) == 0) {
			sscanf(line, "n %d", &m);	
			continue;
		}
		if (NULL == x) {
			x = (double *)malloc(sizeof(double) * n);
			memset(x, 0, sizeof(double) * n);
			r = (double *)malloc(sizeof(double) * m);
			C_ = (double *)malloc(sizeof(double) * m * (n + 1));
      C = (double *)malloc(sizeof(double) * m * n);
		}
		token = strtok(line, " \n");
		for (j = 1; j <= n; j++) {
			sscanf(token, "%lf", &C_[i * (n + 1) + j]);
      C[i * n + j - 1] = C_[i * (n + 1) + j];
			token = strtok(NULL, " \n");
		}
		sscanf(token, "%lf", &C[i * (n + 1)]);
    r[i] = C[i * (n + 1)];
    i++;
	}

	if (x) {
    int *nbd = (int *)malloc(sizeof(int) * n);
    memset(nbd, 0, sizeof(double) * n);
    double *l = (double *)malloc(sizeof(double) * n);
    double *u = (double *)malloc(sizeof(double) * n);
    double f;
    double *g = (double *)malloc(sizeof(double) * n);
    double factr = 1e7;
    double pgtol = 1e-5;
    double *wa = (double *)malloc(sizeof(double) * 10000000);
//                                  ((2 * m + 4) * n + 11 * m * m + 8 * m));
    double *e = (double *)malloc(sizeof(double) * m);
    int *iwa = (int *)malloc(sizeof(int) * 3 * n);
    char task[60];
    int iprint = 0;
    char csave[60];
    logical lsave[4];
    int isave[44];
    double dsave[29];
  
    s_copy(task, "START", (ftnlen)60, (ftnlen)5);
    int s = m;
    double *ci = (double *)malloc(sizeof(double) * n);
    double t1, t2, t3;
    int i, j, i__, i__1, i__2;
    double ps[] = {1e-1, 1e-2, 1e-3, 1e-4};
    double *c__ = C_;
    int q;
	  
    struct timeval  t_begin, t_end;
    double timing;
    gettimeofday(&t_begin, NULL);

    for (q = 1; q <= 6; ++q) {
      double p = ps[q - 1];
      while (1) {
        int lbfgs_m = 7;
        setulb_(&n, &lbfgs_m, x, l, u, nbd, &f, g, &factr, &pgtol, wa, iwa, task, &iprint,
            csave, lsave, isave, dsave, (ftnlen)60, (ftnlen)60);
        if (s_cmp(task, "FG", (ftnlen)2, (ftnlen)2) == 0) {
          t3 = 0.;
          i__1 = s;
          for (i__ = 1; i__ <= i__1; ++i__) {
            t2 = 0.;
            i__2 = n;
            for (j = 1; j <= i__2; ++j) {
              ci[j - 1] = c__[(i__ - 1) * (n + 1) + j];
              ci[j - 1] = x[j - 1] - ci[j - 1];
              t2 += ci[j - 1] * ci[j - 1];
            }
            if (t3 <= sqrt(t2) + c__[(i__ - 1) * (n + 1)]) {
              t3 = sqrt(t2) + c__[(i__ - 1) * (n + 1)];
            }
            t2 += p * p;
            e[i__ - 1] = sqrt(t2) + c__[(i__ - 1) * (n + 1)];
            /* L19: */
          }
          t1 = 0.;
          i__1 = s;
          for (i__ = 1; i__ <= i__1; ++i__) {
            if (t1 <= e[i__ - 1]) {
              t1 = e[i__ - 1];
            }
            /* L20: */
          }
          t2 = 0.;
          i__1 = s;
          for (i__ = 1; i__ <= i__1; ++i__) {
            e[i__ - 1] = exp((e[i__ - 1] - t1) / p);
            t2 += e[i__ - 1];
            /* L21: */
          }
          f = t1 + p * log(t2);
          /*        Compute gradient g of the function f_p. */
          i__1 = s;
          for (i__ = 1; i__ <= i__1; ++i__) {
            e[i__ - 1] /= t2;
            /* L22: */
          }
          i__1 = n;
          for (i__ = 1; i__ <= i__1; ++i__) {
            g[i__ - 1] = 0.;
            /* L23: */
          }
          i__1 = s;
          for (i__ = 1; i__ <= i__1; ++i__) {
            t2 = 0.;
            i__2 = n;
            for (j = 1; j <= i__2; ++j) {
              ci[j - 1] = c__[(i__ - 1) * (n + 1) + j];
              ci[j - 1] = x[j - 1] - ci[j - 1];
              t2 += ci[j - 1] * ci[j - 1];
              /* L24: */
            }
            t2 = sqrt(t2 + p * p);
            i__2 = n;
            for (j = 1; j <= i__2; ++j) {
              g[j - 1] += e[i__ - 1] * ci[j - 1] / t2;
              /* L25: */
            }
            /* L26: */
          }
          continue;
        } else if (s_cmp(task, "NEW_X", (ftnlen)5, (ftnlen)5) == 0) {
          continue;
        }
        break;
      }
     

    }
    gettimeofday(&t_end, NULL);
	  timing = (double)(t_end.tv_sec - t_begin.tv_sec + (t_end.tv_usec -
                                               t_begin.tv_usec)/1000000.0);
	  printf("time  %.10e  \n", timing);
    truncate_newton_cg(m, n, x, r, C);
    gettimeofday(&t_end, NULL);
	  timing = (double)(t_end.tv_sec - t_begin.tv_sec + (t_end.tv_usec -
                                               t_begin.tv_usec)/1000000.0);
		
	  printf("time  %.10e  \n", timing);
/*
	  clock_t t1 = clock(), t2;
    double timing;
		y = truncate_newton_cg(m, n, x, r, C);
    t2 = clock();
	  timing = (double) (t2 - t1) / (double) CLOCKS_PER_SEC;
	  printf("%8d%8d  %.10e  %.10e\n", m, n, timing, y);

		for (i = 0; i < n; i++) {
			printf("%.6f\n", x[i]);
		}
		printf("y = %.6f\n", y);
		free(x);
		free(r);
		free(C);*/
	}
	fclose(f);
}


int MAIN__() {
  test_bls("../Data/Bunny.bls");
  // test_bls_with_lbfgs("../Data/Bunny.bls");
	return 0;
}

